<?php

namespace App\Http\Controllers\CV;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CV_Candidate;
use App\Models\work_experience;
use App\Models\aspiration_find_job;
use App\Models\qualification;
use App\Models\care_candidate;
use Validator;

class CV_CandidateController extends Controller
{
    //
    public function showInfoCV()
    {
        $data = CV_Candidate::limit(50)->get();
        $count = CV_Candidate::limit(50)->get()->count();
        return response()->json([
            'itemCount' => $count,
            'data' => $data,
        ]);
    }
    public function addCare(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'idCandidate' => 'required',
            'idRecruiter' => 'required',
            'care' => 'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'message' => $validator->errors()
            ], 400);
        }
        $add = care_candidate::create(array_merge(
            $validator->validated()
        ));

        return response()->json([
            'message' => 'Add care candidate successfully ',
            'user' => $add
        ], 201);
    }

    public function addDontCare(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'idCandidate' => 'required',
            'idRecruiter' => 'required',
            'care' => 'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'message' => $validator->errors()
            ], 400);
        }
        $add = care_candidate::create(array_merge(
            $validator->validated()
        ));

        return response()->json([
            'message' => 'Add dont care candidate successfully ',
            'user' => $add
        ], 201);
    }

    public function showCV()
    {
        $data = CV_Candidate::join('qualifications', 'qualifications.idCandidate', '=', 'c_v__candidates.id')
        ->join('work_experiences', 'work_experiences.idCandidate', '=', 'c_v__candidates.id')
        ->join('aspiration_find_jobs', 'aspiration_find_jobs.idCandidate', '=', 'c_v__candidates.id')->limit(50)
        ->inRandomOrder()
        ->get(['c_v__candidates.id', 'c_v__candidates.full_name', 'c_v__candidates.birthday', 'c_v__candidates.gender', 'c_v__candidates.marital_status', 'c_v__candidates.region', 'c_v__candidates.city', 'c_v__candidates.district', 'c_v__candidates.address', 'work_experiences.years_experience', 'aspiration_find_jobs.fields', 'aspiration_find_jobs.desired_min_salary', 'aspiration_find_jobs.desired_max_salary', 'aspiration_find_jobs.type_work', 'aspiration_find_jobs.level_desired']);
        $count = CV_Candidate::join('qualifications', 'qualifications.idCandidate', '=', 'c_v__candidates.id')
        ->join('work_experiences', 'work_experiences.idCandidate', '=', 'c_v__candidates.id')
        ->join('aspiration_find_jobs', 'aspiration_find_jobs.idCandidate', '=', 'c_v__candidates.id')->limit(50)
        ->inRandomOrder()
        ->get(['c_v__candidates.id', 'c_v__candidates.full_name', 'c_v__candidates.birthday', 'c_v__candidates.gender', 'c_v__candidates.marital_status', 'c_v__candidates.region', 'c_v__candidates.city', 'c_v__candidates.district', 'c_v__candidates.address', 'work_experiences.years_experience', 'aspiration_find_jobs.fields', 'aspiration_find_jobs.desired_min_salary', 'aspiration_find_jobs.desired_max_salary', 'aspiration_find_jobs.type_work', 'aspiration_find_jobs.level_desired'])->count();
        return response()->json([
            'itemCount' => $count,
            'data' => $data,
        ]);
    }
    public function showCareCV(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'idRecruiter' => 'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'message' => $validator->errors()
            ], 400);
        }

        $input = $request->all();
        $data = CV_Candidate::join('qualifications', 'qualifications.idCandidate', '=', 'c_v__candidates.id')
        ->join('work_experiences', 'work_experiences.idCandidate', '=', 'c_v__candidates.id')
        ->join('aspiration_find_jobs', 'aspiration_find_jobs.idCandidate', '=', 'c_v__candidates.id')->limit(50)
        ->join('care_candidates', 'care_candidates.idCandidate', '=', 'c_v__candidates.id')
        ->where('care', 1)
        ->where('idRecruiter', $input['idRecruiter'])
        ->get(['c_v__candidates.id', 'c_v__candidates.full_name', 'c_v__candidates.birthday', 'c_v__candidates.gender', 'c_v__candidates.marital_status', 'c_v__candidates.region', 'c_v__candidates.city', 'c_v__candidates.district', 'c_v__candidates.address', 'work_experiences.years_experience', 'aspiration_find_jobs.fields', 'aspiration_find_jobs.desired_min_salary', 'aspiration_find_jobs.desired_max_salary', 'aspiration_find_jobs.type_work', 'aspiration_find_jobs.level_desired']);
        $count = CV_Candidate::join('qualifications', 'qualifications.idCandidate', '=', 'c_v__candidates.id')
        ->join('work_experiences', 'work_experiences.idCandidate', '=', 'c_v__candidates.id')
        ->join('aspiration_find_jobs', 'aspiration_find_jobs.idCandidate', '=', 'c_v__candidates.id')->limit(50)
        ->join('care_candidates', 'care_candidates.idCandidate', '=', 'c_v__candidates.id')
        ->where('care', 1)
        ->where('idRecruiter', $input['idRecruiter'])
        ->get(['c_v__candidates.id', 'c_v__candidates.full_name', 'c_v__candidates.birthday', 'c_v__candidates.gender', 'c_v__candidates.marital_status', 'c_v__candidates.region', 'c_v__candidates.city', 'c_v__candidates.district', 'c_v__candidates.address', 'work_experiences.years_experience', 'aspiration_find_jobs.fields', 'aspiration_find_jobs.desired_min_salary', 'aspiration_find_jobs.desired_max_salary', 'aspiration_find_jobs.type_work', 'aspiration_find_jobs.level_desired'])->count();
        return response()->json([
            'itemCount' => $count,
            'data' => $data,
        ]);
    }
    public function showDontCareCV(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'idRecruiter' => 'required'
        ]);
        if($validator->fails()){
            return response()->json([
                'message' => $validator->errors()
            ], 400);
        }

        $data = CV_Candidate::join('qualifications', 'qualifications.idCandidate', '=', 'c_v__candidates.id')
        ->join('work_experiences', 'work_experiences.idCandidate', '=', 'c_v__candidates.id')
        ->join('aspiration_find_jobs', 'aspiration_find_jobs.idCandidate', '=', 'c_v__candidates.id')->limit(50)
        ->join('care_candidates', 'care_candidates.idCandidate', '=', 'c_v__candidates.id')
        ->where('care', 0)
        ->where('idRecruiter', $input['idRecruiter'])
        ->get(['c_v__candidates.id', 'c_v__candidates.full_name', 'c_v__candidates.birthday', 'c_v__candidates.gender', 'c_v__candidates.marital_status', 'c_v__candidates.region', 'c_v__candidates.city', 'c_v__candidates.district', 'c_v__candidates.address', 'work_experiences.years_experience', 'aspiration_find_jobs.fields', 'aspiration_find_jobs.desired_min_salary', 'aspiration_find_jobs.desired_max_salary', 'aspiration_find_jobs.type_work', 'aspiration_find_jobs.level_desired']);
        $count = CV_Candidate::join('qualifications', 'qualifications.idCandidate', '=', 'c_v__candidates.id')
        ->join('work_experiences', 'work_experiences.idCandidate', '=', 'c_v__candidates.id')
        ->join('aspiration_find_jobs', 'aspiration_find_jobs.idCandidate', '=', 'c_v__candidates.id')->limit(50)
        ->join('care_candidates', 'care_candidates.idCandidate', '=', 'c_v__candidates.id')
        ->where('care', 0)
        ->where('idRecruiter', $input['idRecruiter'])
        ->get(['c_v__candidates.id', 'c_v__candidates.full_name', 'c_v__candidates.birthday', 'c_v__candidates.gender', 'c_v__candidates.marital_status', 'c_v__candidates.region', 'c_v__candidates.city', 'c_v__candidates.district', 'c_v__candidates.address', 'work_experiences.years_experience', 'aspiration_find_jobs.fields', 'aspiration_find_jobs.desired_min_salary', 'aspiration_find_jobs.desired_max_salary', 'aspiration_find_jobs.type_work', 'aspiration_find_jobs.level_desired'])->count();
        return response()->json([
            'itemCount' => $count,
            'data' => $data,
        ]);
    }
    public function recommendCV(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'recommend' => 'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'message' => $validator->errors()
            ], 400);
        }

        $input = $request->all();
        if(str_contains($input['recommend'], '('))
        {
            $a = strpos($input['recommend'], '(') ;
            $b = (int)$a;
            $title1 = substr($input['recommend'], 0, $b);
        
            $c = strpos($input['recommend'], ')');
            $d = (int)$c;
        
            $title2 = substr($input['recommend'], $b+1, -1);
            //echo $title1 . "\n". $title2;
            
            $data = CV_Candidate::join('qualifications', 'qualifications.idCandidate', '=', 'c_v__candidates.id')
            ->join('work_experiences', 'work_experiences.idCandidate', '=', 'c_v__candidates.id')
            ->join('aspiration_find_jobs', 'aspiration_find_jobs.idCandidate', '=', 'c_v__candidates.id')->limit(50)
            ->where('fields', 'like', '%'.$title1.'%')
            ->orWhere('fields', 'like', '%'.$title2.'%')
            ->inRandomOrder()
            ->get(['c_v__candidates.id', 'c_v__candidates.full_name', 'c_v__candidates.birthday', 'c_v__candidates.gender', 'c_v__candidates.marital_status', 'c_v__candidates.region', 'c_v__candidates.city', 'c_v__candidates.district', 'c_v__candidates.address', 'work_experiences.years_experience', 'aspiration_find_jobs.fields', 'aspiration_find_jobs.desired_min_salary', 'aspiration_find_jobs.desired_max_salary', 'aspiration_find_jobs.type_work', 'aspiration_find_jobs.level_desired']);
            $count = CV_Candidate::join('qualifications', 'qualifications.idCandidate', '=', 'c_v__candidates.id')
            ->join('work_experiences', 'work_experiences.idCandidate', '=', 'c_v__candidates.id')
            ->join('aspiration_find_jobs', 'aspiration_find_jobs.idCandidate', '=', 'c_v__candidates.id')->limit(50)
            ->where('fields', 'like', '%'.$title1.'%')
            ->orWhere('fields', 'like', '%'.$title2.'%')
            ->inRandomOrder()
            ->get(['c_v__candidates.id', 'c_v__candidates.full_name', 'c_v__candidates.birthday', 'c_v__candidates.gender', 'c_v__candidates.marital_status', 'c_v__candidates.region', 'c_v__candidates.city', 'c_v__candidates.district', 'c_v__candidates.address', 'work_experiences.years_experience', 'aspiration_find_jobs.fields', 'aspiration_find_jobs.desired_min_salary', 'aspiration_find_jobs.desired_max_salary', 'aspiration_find_jobs.type_work', 'aspiration_find_jobs.level_desired'])->count();
            if($count < 5)
            {
                //echo $input['recommend']. ' - ';
                
                $array = explode(" ",$title1);
                //print_r ($array);
                if(count($array) >1){
                    $title3 = $array[0] . " " . $array[1];
                   // echo $title3. " - ";
                }
                else{
                    $title3 = $array[0];
                    //echo $title3. " - ";
                }
                $title4 = end($array);
                //echo $title4. " - ";
                $array2 = explode(" ",$title2);
                //print_r ($array);
                if(count($array2) >1){
                    $title5 = $array2[0] . " " . $array2[1];
                  //  echo $title5. " - ";
                }
                else{
                    $title5 = $array2[0];
                    //echo $title5 . " - ";
                }
                $title6 = end($array2);
                //echo $title6;
                
                $data = CV_Candidate::join('qualifications', 'qualifications.idCandidate', '=', 'c_v__candidates.id')
            ->join('work_experiences', 'work_experiences.idCandidate', '=', 'c_v__candidates.id')
            ->join('aspiration_find_jobs', 'aspiration_find_jobs.idCandidate', '=', 'c_v__candidates.id')->limit(50)
            ->where('fields', 'like', '%'.$title3.'%')
            ->orWhere('fields', 'like', '%'.$title4.'%')
            ->orWhere('fields', 'like', '%'.$title5.'%')
            ->orWhere('fields', 'like', '%'.$title6.'%')
            ->inRandomOrder()
            ->get(['c_v__candidates.id', 'c_v__candidates.full_name', 'c_v__candidates.birthday', 'c_v__candidates.gender', 'c_v__candidates.marital_status', 'c_v__candidates.region', 'c_v__candidates.city', 'c_v__candidates.district', 'c_v__candidates.address', 'work_experiences.years_experience', 'aspiration_find_jobs.fields', 'aspiration_find_jobs.desired_min_salary', 'aspiration_find_jobs.desired_max_salary', 'aspiration_find_jobs.type_work', 'aspiration_find_jobs.level_desired']);
            $count = CV_Candidate::join('qualifications', 'qualifications.idCandidate', '=', 'c_v__candidates.id')
            ->join('work_experiences', 'work_experiences.idCandidate', '=', 'c_v__candidates.id')
            ->join('aspiration_find_jobs', 'aspiration_find_jobs.idCandidate', '=', 'c_v__candidates.id')->limit(50)
            ->where('fields', 'like', '%'.$title3.'%')
            ->orWhere('fields', 'like', '%'.$title4.'%')
            ->orWhere('fields', 'like', '%'.$title5.'%')
            ->orWhere('fields', 'like', '%'.$title6.'%')
            ->inRandomOrder()
            ->get(['c_v__candidates.id', 'c_v__candidates.full_name', 'c_v__candidates.birthday', 'c_v__candidates.gender', 'c_v__candidates.marital_status', 'c_v__candidates.region', 'c_v__candidates.city', 'c_v__candidates.district', 'c_v__candidates.address', 'work_experiences.years_experience', 'aspiration_find_jobs.fields', 'aspiration_find_jobs.desired_min_salary', 'aspiration_find_jobs.desired_max_salary', 'aspiration_find_jobs.type_work', 'aspiration_find_jobs.level_desired'])->count();
                return response()->json([
                    'itemCount' => $count,
                    'data' => $data,
                ]);
            }
            else{
                return response()->json([
                    'itemCount' => $count,
                    'data' => $data,
                ]);
            }
            
        }
        else {  
            $data = CV_Candidate::join('qualifications', 'qualifications.idCandidate', '=', 'c_v__candidates.id')
            ->join('work_experiences', 'work_experiences.idCandidate', '=', 'c_v__candidates.id')
            ->join('aspiration_find_jobs', 'aspiration_find_jobs.idCandidate', '=', 'c_v__candidates.id')->limit(50)
            ->where('fields', 'like', '%'.$input['recommend'].'%')
            ->inRandomOrder()
            ->get(['c_v__candidates.id', 'c_v__candidates.full_name', 'c_v__candidates.birthday', 'c_v__candidates.gender', 'c_v__candidates.marital_status', 'c_v__candidates.region', 'c_v__candidates.city', 'c_v__candidates.district', 'c_v__candidates.address', 'work_experiences.years_experience', 'aspiration_find_jobs.fields', 'aspiration_find_jobs.desired_min_salary', 'aspiration_find_jobs.desired_max_salary', 'aspiration_find_jobs.type_work', 'aspiration_find_jobs.level_desired']);
            $count = CV_Candidate::join('qualifications', 'qualifications.idCandidate', '=', 'c_v__candidates.id')
            ->join('work_experiences', 'work_experiences.idCandidate', '=', 'c_v__candidates.id')
            ->join('aspiration_find_jobs', 'aspiration_find_jobs.idCandidate', '=', 'c_v__candidates.id')->limit(50)
            ->where('fields', 'like', '%'.$input['recommend'].'%')
            ->inRandomOrder()
            ->get(['c_v__candidates.id', 'c_v__candidates.full_name', 'c_v__candidates.birthday', 'c_v__candidates.gender', 'c_v__candidates.marital_status', 'c_v__candidates.region', 'c_v__candidates.city', 'c_v__candidates.district', 'c_v__candidates.address', 'work_experiences.years_experience', 'aspiration_find_jobs.fields', 'aspiration_find_jobs.desired_min_salary', 'aspiration_find_jobs.desired_max_salary', 'aspiration_find_jobs.type_work', 'aspiration_find_jobs.level_desired'])->count();
            if($count < 5)
            {
                //echo $input['recommend']. ' - ';
                
                $array = explode(" ",$input['recommend']);
                //print_r ($array);
                if(count($array) >1)
                    $title3 = $array[0] . " " . $array[1];
                    
                else{
                    $title3 = $array[0];
                }
                $title4 = end($array);
                //echo $title3;
                
                $data = CV_Candidate::join('qualifications', 'qualifications.idCandidate', '=', 'c_v__candidates.id')
            ->join('work_experiences', 'work_experiences.idCandidate', '=', 'c_v__candidates.id')
            ->join('aspiration_find_jobs', 'aspiration_find_jobs.idCandidate', '=', 'c_v__candidates.id')->limit(50)
            ->where('fields', 'like', '%'.$title3.'%')
            ->orWhere('fields', 'like', '%'.$title4.'%')
            ->inRandomOrder()
            ->get(['c_v__candidates.id', 'c_v__candidates.full_name', 'c_v__candidates.birthday', 'c_v__candidates.gender', 'c_v__candidates.marital_status', 'c_v__candidates.region', 'c_v__candidates.city', 'c_v__candidates.district', 'c_v__candidates.address', 'work_experiences.years_experience', 'aspiration_find_jobs.fields', 'aspiration_find_jobs.desired_min_salary', 'aspiration_find_jobs.desired_max_salary', 'aspiration_find_jobs.type_work', 'aspiration_find_jobs.level_desired']);
            $count = CV_Candidate::join('qualifications', 'qualifications.idCandidate', '=', 'c_v__candidates.id')
            ->join('work_experiences', 'work_experiences.idCandidate', '=', 'c_v__candidates.id')
            ->join('aspiration_find_jobs', 'aspiration_find_jobs.idCandidate', '=', 'c_v__candidates.id')->limit(50)
            ->where('fields', 'like', '%'.$title3.'%')
            ->orWhere('fields', 'like', '%'.$title4.'%')
            ->inRandomOrder()
            ->get(['c_v__candidates.id', 'c_v__candidates.full_name', 'c_v__candidates.birthday', 'c_v__candidates.gender', 'c_v__candidates.marital_status', 'c_v__candidates.region', 'c_v__candidates.city', 'c_v__candidates.district', 'c_v__candidates.address', 'work_experiences.years_experience', 'aspiration_find_jobs.fields', 'aspiration_find_jobs.desired_min_salary', 'aspiration_find_jobs.desired_max_salary', 'aspiration_find_jobs.type_work', 'aspiration_find_jobs.level_desired'])->count();
                return response()->json([
                    'itemCount' => $count,
                    'data' => $data,
                ]);
            }
            else {
                return response()->json([
                    'itemCount' => $count,
                    'data' => $data,
                ]);
            }
            
        }
    }
    public function searchCV(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'search' => 'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'message' => $validator->errors()
            ], 400);
        }

        $input = $request->all();
        if(str_contains($input['search'], '('))
        {
            $a = strpos($input['search'], '(');
            $b = (int)$a;
            $title1 = substr($input['search'], 0, $b);
        
            $c = strpos($input['search'], ')');
            $d = (int)$c;
        
            $title2 = substr($input['search'], $b+1, -1);
            //echo $title1 . $title2;
            $data = CV_Candidate::join('qualifications', 'qualifications.idCandidate', '=', 'c_v__candidates.id')
            ->join('work_experiences', 'work_experiences.idCandidate', '=', 'c_v__candidates.id')
            ->join('aspiration_find_jobs', 'aspiration_find_jobs.idCandidate', '=', 'c_v__candidates.id')->limit(100)
            ->where('fields', 'like', '%'.$title1.'%')
            ->orWhere('fields', 'like', '%'.$title2.'%')
            ->get(['c_v__candidates.id', 'c_v__candidates.full_name', 'c_v__candidates.birthday', 'c_v__candidates.gender', 'c_v__candidates.marital_status', 'c_v__candidates.region', 'c_v__candidates.city', 'c_v__candidates.district', 'c_v__candidates.address', 'work_experiences.years_experience', 'aspiration_find_jobs.fields', 'aspiration_find_jobs.desired_min_salary', 'aspiration_find_jobs.desired_max_salary', 'aspiration_find_jobs.type_work', 'aspiration_find_jobs.level_desired']);
            $count = CV_Candidate::join('qualifications', 'qualifications.idCandidate', '=', 'c_v__candidates.id')
            ->join('work_experiences', 'work_experiences.idCandidate', '=', 'c_v__candidates.id')
            ->join('aspiration_find_jobs', 'aspiration_find_jobs.idCandidate', '=', 'c_v__candidates.id')->limit(100)
            ->where('fields', 'like', '%'.$title1.'%')
            ->orWhere('fields', 'like', '%'.$title2.'%')
            ->get(['c_v__candidates.id', 'c_v__candidates.full_name', 'c_v__candidates.birthday', 'c_v__candidates.gender', 'c_v__candidates.marital_status', 'c_v__candidates.region', 'c_v__candidates.city', 'c_v__candidates.district', 'c_v__candidates.address', 'work_experiences.years_experience', 'aspiration_find_jobs.fields', 'aspiration_find_jobs.desired_min_salary', 'aspiration_find_jobs.desired_max_salary', 'aspiration_find_jobs.type_work', 'aspiration_find_jobs.level_desired'])->count();
            return response()->json([
                'itemCount' => $count,
                'data' => $data,
            ]);
        }
        else {
            $data = CV_Candidate::join('qualifications', 'qualifications.idCandidate', '=', 'c_v__candidates.id')
            ->join('work_experiences', 'work_experiences.idCandidate', '=', 'c_v__candidates.id')
            ->join('aspiration_find_jobs', 'aspiration_find_jobs.idCandidate', '=', 'c_v__candidates.id')->limit(100)
            ->where('fields', 'like', '%'.$input['search'].'%')
            ->get(['c_v__candidates.id', 'c_v__candidates.full_name', 'c_v__candidates.birthday', 'c_v__candidates.gender', 'c_v__candidates.marital_status', 'c_v__candidates.region', 'c_v__candidates.city', 'c_v__candidates.district', 'c_v__candidates.address', 'work_experiences.years_experience', 'aspiration_find_jobs.fields', 'aspiration_find_jobs.desired_min_salary', 'aspiration_find_jobs.desired_max_salary', 'aspiration_find_jobs.type_work', 'aspiration_find_jobs.level_desired']);
            $count = CV_Candidate::join('qualifications', 'qualifications.idCandidate', '=', 'c_v__candidates.id')
            ->join('work_experiences', 'work_experiences.idCandidate', '=', 'c_v__candidates.id')
            ->join('aspiration_find_jobs', 'aspiration_find_jobs.idCandidate', '=', 'c_v__candidates.id')->limit(100)
            ->where('fields', 'like', '%'.$input['search'].'%')
            ->get(['c_v__candidates.id', 'c_v__candidates.full_name', 'c_v__candidates.birthday', 'c_v__candidates.gender', 'c_v__candidates.marital_status', 'c_v__candidates.region', 'c_v__candidates.city', 'c_v__candidates.district', 'c_v__candidates.address', 'work_experiences.years_experience', 'aspiration_find_jobs.fields', 'aspiration_find_jobs.desired_min_salary', 'aspiration_find_jobs.desired_max_salary', 'aspiration_find_jobs.type_work', 'aspiration_find_jobs.level_desired'])->count();
            return response()->json([
                'itemCount' => $count,
                'data' => $data,
            ]);
        }
 
        
    }
}
