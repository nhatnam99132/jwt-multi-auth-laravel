<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\AuthController;
use App\Http\Controllers\Candidate\CandidateAuthController;
use App\Http\Controllers\CV\CV_CandidateController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'user'

], function ($router) {
    Route::post('/login', [AuthController::class, 'login'])->name('user.login');
    Route::post('/register', [AuthController::class, 'register'])->name('user.register');
    Route::post('/logout', [AuthController::class, 'logout'])->name('user.logout');
    Route::post('/refresh', [AuthController::class, 'refresh'])->name('user.refresh');
    Route::get('/user-profile', [AuthController::class, 'userProfile'])->name('user.userProfile');
    Route::post('/updateinfo', [AuthController::class, 'updateInfo'])->name('user.updateinfo');  
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'candidate'

], function ($router) {
    Route::post('/login', [CandidateAuthController::class, 'login'])->name('candidate.login');
    Route::post('/register', [CandidateAuthController::class, 'register'])->name('candidate.register');
    Route::post('/logout', [CandidateAuthController::class, 'logout'])->name('candidate.logout');
    Route::post('/refresh', [CandidateAuthController::class, 'refresh'])->name('candidate.refresh');
    Route::get('/user-profile', [CandidateAuthController::class, 'userProfile'])->name('user.userProfile');    
});
Route::get('/infocv', [CV_CandidateController::class, 'showInfoCV'])->name('cv.showInfoCV');
Route::get('/list', [CV_CandidateController::class, 'showCV'])->name('cv.showCV');
Route::post('/carecv', [CV_CandidateController::class, 'showCareCV'])->name('cv.showCareCV');
Route::post('/dontcarecv', [CV_CandidateController::class, 'showDontCareCV'])->name('cv.showDontCareCV');
Route::post('/addcare', [CV_CandidateController::class, 'addCare'])->name('cv.addcare');
Route::post('/adddontcare', [CV_CandidateController::class, 'addDontCare'])->name('cv.adddontcare');
Route::post('/recommendcv', [CV_CandidateController::class, 'recommendCV'])->name('cv.recommendcv');
Route::post('/searchcv', [CV_CandidateController::class, 'searchCV'])->name('cv.searchcv');