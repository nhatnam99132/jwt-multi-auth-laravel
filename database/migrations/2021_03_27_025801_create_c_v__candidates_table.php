<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCVCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c_v__candidates', function (Blueprint $table) {
            $table->id();
            $table->string('full_name');
            $table->string('birthday');
            $table->string('phone_number');
            $table->string('email');
            $table->string('gender');
            $table->string('marital_status');
            $table->string('region');
            $table->string('city');
            $table->string('district');
            $table->string('address');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c_v__candidates');
    }
}
