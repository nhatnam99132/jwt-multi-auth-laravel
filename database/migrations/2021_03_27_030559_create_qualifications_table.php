<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQualificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qualifications', function (Blueprint $table) {
            $table->id();
            $table->string('name_degree');
            $table->string('date_of_issue');
            $table->string('effective_time');
            $table->string('issuing_organization');
            $table->string('national_release');
            $table->string('form_study');
            $table->bigInteger('idCandidate')->unsigned()->nullable();
            $table->foreign('idCandidate')->references('id')->on('c_v__candidates')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qualifications');
    }
}
