<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAspirationFindJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aspiration_find_jobs', function (Blueprint $table) {
            $table->id();
            $table->string('fields');
            $table->string('desired_min_salary');
            $table->string('desired_max_salary');
            $table->string('type_work');
            $table->string('level_desired');
            $table->bigInteger('idCandidate')->unsigned()->nullable();
            $table->foreign('idCandidate')->references('id')->on('c_v__candidates')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aspiration_find_jobs');
    }
}
